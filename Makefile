CFLAGS=`pkg-config --cflags libcrypto` -Wall -Wextra
LDFLAGS=`pkg-config --libs libcrypto`

mxssb:	mxssb.c
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

clean:
	rm -f mxssb
